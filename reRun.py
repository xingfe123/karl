#!/usr/bin/python
# -*- coding: utf-8 -*-
import MySQLdb as dbapi
import datetime
import requests
import click
import sys
import os

path = os.path.abspath(__file__)
dir_path = os.path.dirname(path)
sys.path.append(dir_path)
import ClearData


@click.group()
def cli():
    pass


#dbServer = '10.8.18.102'
dbServer = '10.8.18.213'
dbPass = '1234'
dbSchema = 'posbill'
dbUser = 'root'


def getCur():
    db = dbapi.connect(host=dbServer, user=dbUser, passwd=dbPass, db=dbSchema)
    cur = db.cursor()
    return cur

def Execte(sql):
    cur = getCur()
    # print(sql)
    cur.execute(sql)
    result = cur.fetchall()

    return result

def ExecuteFile(fileName):
    db = dbapi.connect(host=dbServer, user=dbUser, passwd=dbPass, db=dbSchema)
    cursor = db.cursor()
    for line in open(fileName):
        cursor.execute(line)
        print(cursor.fetchone())
    db.close()

###########################


def getState1Result(date):
    return getState(date, 10)


def getState(date, agentId):
    sql= "select F_state from posbill.t_agent_profit_log where" +\
         " F_agent_id={agentId} and F_date='{date}'" .format(date=date, agentId = agentId)

    for row in Execte(sql):
        return int(row[0])
    return None


def getState2Result(date):
    return getState(date, 11)


class State():
    def __init__(self, date, message):
        self.date = date
        self.message = message
        self.state1 = None
        self.state2 = None

    # def date2(self):
    #     return formatDate2(self.date)

    def isPreFailed(self):
        return self.state1 is not None \
            and self.state1 == 2 \
            and self.state2 is None

    def isSettlementFailed(self):
        return self.state1 is not None \
            and self.state2 is not None\
            and self.state2 == 2

    def isworking(self):
        if self.failed():
            return False

        if self.state1 is not None \
            and self.state2 is not None\
            and self.state2 == 0:
            return True

        if self.state1 is not None \
            and self.state2 is None\
            and self.state1 == 0:
            return True


        return False

    def failed(self):
        return self.isSettlementFailed() or \
            self.isPreFailed()

    def noStart(self):
        return self.message == "no start"

    def state1Succ(self):
        return self.message == 'state1 succ'

    def state2Succ(self):
        return self.message == 'state2 succ'

    def setState(self, state1, state2):
        self.state1 = state1
        self.state2 = state2


def getStateResult(date):
    state1 = getState1Result(date)
    state2 = getState2Result(date)

    if state1 is None and state2 is None:
        s =  State(date, "no start")

    elif state1 == 1 and state2 is None:
        s = State(date, "state1 succ")

    elif state1 == 1 and state2 == 1:
        s = State(date, "state2 succ")
    elif state1 == 0:
        s = State(date, "state1 working")
    else:
        s = State(date, "failed")
    s.setState(state1, state2)
    return s


def fixStatementReRun(date):
    print("python create_rerun_sql.py 14 {start} {end} 6".format(
        start=date, end=date))
    ClearData.pymain('14-{date}.sql'.format(date=date), date, date, 14, 6)


def fixPreStatementReRun(date):
    print("python create_rerun_sql.py 13 {start} {end} 6".format(
        start=date, end=date))
    ClearData.pymain('13-{date}.sql'.format(date=date), date, date, 13, 6)


def failedAction(state):
    assert (state.state1 is not None)
    print("Failed", state.date, state.state1, state.state2)
    if state.state1 == 1 and state.state2 != 1:
        fixStatementReRun(state.date)
    elif state.state1 != 1:
        fixPreStatementReRun(state.date)
    else:
        assert (False)


def state2SuccAction(date):
    print("succ", date)


def formatDate(date):
    start_date = datetime.datetime.strptime(date, '%Y-%m-%d')
    dateStr = start_date.strftime('%Y%m%d')
    return dateStr

def formatDate2(date):
    start_date = datetime.datetime.strptime(date, '%Y%m%d')
    dateStr = start_date.strftime('%Y-%m-%d')
    return dateStr


def sendRequest(cmd, date):
    payload = {'cmd': cmd, 'settle_date': formatDate(date)}
    r = requests.get(
        "http://10.8.18.213/cgi-bin/lepos_agentprofitserver.cgi",
        params=payload,
        timeout=5000)
    print(r.url)


def state1SuccAction(date):
    sendRequest('SB_AGENT_SETTLEMENT', date)
    print("SB_AGENT_SETTLEMENT", formatDate(date))


def noStartAction(date):
    sendRequest('SB_AGENT_PRE_SETTLEMENT', date)
    print("SB_AGENT_PRE_SETTLEMENT", formatDate(date))


def pymain():
    start = "20180810"
    start_date = datetime.datetime.strptime(start, '%Y%m%d')
    datelist = [start_date + datetime.timedelta(days=x) for x in range(0, 32)]
    #datelist = pd.date_range(start, periods=30).tolist()
    states = []
    for date in datelist:
        dateStr = date.strftime('%Y-%m-%d')
        print(dateStr)
        states.append(getStateResult(dateStr))

    failedStates = []
    preAction = []
    settlement = []
    for state in states:
        if state.failed():
            failedStates.append(state)
        elif state.state1Succ():
            settlement.append(state)

        elif state.state2Succ():
            state2SuccAction(state.date)
        elif state.noStart():
            preAction.append(state)
        elif state.isworking():
            print(state)
        else:
            raise Exception('failed1 ', state.date, state.message,
                            state.state1, state.state2)
    return failedStates, preAction, settlement


@click.command()
def query():
    failedStates, preAction, settlement = pymain()
    for state in failedStates:
        print(state.state1, state.state2, state.date)

    for preAction in preAction:
        print(preAction.state1, preAction.state2)

    for action in settlement:
        print(action.state1, action.state2, action.date)


@click.command()
def action():
    failedStates, preAction, settlement = pymain()


@click.command()
def fixed():
    failedStates, preAction, settlement = pymain()
    for dbRequest in failedStates:
        print('fiexed', dbRequest.date);
        if dbRequest.isPreFailed():
            fileName=dbRequest.date + '-13-test.sql'
            ClearData.pymain(fileName, dbRequest.date,
                             dbRequest.date, "13", "6")
            ExecuteFile(fileName)
        elif dbRequest.isSettlementFailed():
            fileName=dbRequest.date + '-14-test.sql'
            ClearData.pymain(fileName, dbRequest.date,
                             dbRequest.date, "14", "6")
            ExecuteFile(fileName)


cli.add_command(query)
cli.add_command(action)
cli.add_command(fixed)

#python create_rerun_sql.py   13  开始日期 结束日期   6
#  agent_type  = argv[4]
#  g_agent_type = agent_type;
# g_clear_type = sys.argv[1]
# python create_rerun_sql.py   13  开始日期 结束日期   6

if __name__ == '__main__':
    cli()
