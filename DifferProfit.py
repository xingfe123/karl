import csv


class DiffWriter(object):
    def __init__(self, filename):
        self.filename = filename
        self.f = open(self.filename, "wba")
        self.writer = csv.writer(self.f)

    def __exit__(self):
        self.f.close()

    def close(self):
        self.f.close()

    def writeRow(self, row):
        self.writer.writerow(row)

    def writeHead(self, dct):
        self.writer.writerow(dct.keys())


def makeDiffer(makeEmptyDict, diffProfit):
    def process(start, end):
        writer = DiffWriter('tmpdata/t_agent_profit_statement.csv')
        writer.writeHead(makeEmptyDict())
        for table in agentProfitStatementTables():
            diffProfit(writer, table, start, end)
        writer.close()

    return process


def agentProfitStatementTables():
    return tables('t_agent_profit_statement_')


def tables(table_base):
    lst = []
    for i in range(0, 100):
        table_index = "%02d" % int(i)
        lst.append(table_base + table_index)
    return lst
