import MySQLdb as dbapi
import datetime
import requests
import click
import sys
import os
import redis

client = redis.Redis(host='localhost', port=6379, db=0)
path = os.path.abspath(__file__)
dir_path = os.path.dirname(path)
sys.path.append(dir_path)
import ClearData
import DifferProfit
import SqlManager

@click.group()
def cli():
    pass


#dbServer = '10.8.18.102'
dbServer = '10.8.18.213'
dbPass = '1234'
dbSchema = 'posbill'
dbUser = 'root'


def profitStatementTables():
    pass


def makeProfitEmptyDict():
    dct = {}
    dct['F_transaction_id'] = 0
    dct['F_operation_type'] = 0
    dct['F_order_id'] = 0
    dct['F_agent_id'] = 0
    dct['F_merchant_id'] = 0
    dct['F_buy_terminal_level'] = 0
    dct['F_payment_channel_id'] = 0
    dct['F_payment_flag'] = 0
    dct['F_merchant_verified_type'] = 0
    dct['F_agent_verified_type'] = 0
    dct['F_is_bind_sn'] = 0
    dct['F_card_type'] = 0
    dct['F_agent_type'] = 0
    dct['F_profit_rule_type'] = 0
    dct['F_pay_type'] = 0
    dct['F_profit_type'] = 0
    dct['F_flag'] = 0
    dct['F_vip_flag'] = 0
    dct['F_activity_index'] = 0
    dct['F_scheme'] = 0
    dct['F_preferential_type'] = 0
    dct['F_max_flag'] = 0
    dct['F_weixin_activity_flag'] = 0
    dct['F_merchant_commission_bymillion'] = 0
    dct['F_merchant_max_fee_bymillion'] = 0
    dct['F_t0_merchant_fixed_fee_bymillion'] = 0
    dct['F_t1_merchant_fixed_fee_bymillion'] = 0
    dct['F_cost_commission_bymillion'] = 0
    dct['F_cost_max_fee_bymillion'] = 0
    dct['F_t0_cost_fixed_fee_bymillion'] = 0
    dct['F_t1_cost_fixed_fee_bymillion'] = 0
    dct['F_extra_fee_rate_bymillion'] = 0
    dct['F_ins_amount_bymillion'] = 0
    dct['F_payment_source_type'] = 0
    dct['F_amount'] = 0
    dct['F_merchant_fee_bymillion'] = 0
    dct['F_channel_fee_bymillion'] = 0
    dct['F_cost_fee_bymillion'] = 0
    dct['F_profit_bymillion'] = 0
    dct['F_profit_leshua_bymillion'] = 0
    dct['F_time'] = 0
    dct['F_create_time'] = 0
    dct['F_remark'] = 0

    dct['F_new_merchant_fee_bymillion'] = 0
    dct['F_new_profit_bymillion'] = 0

    return dct


def loadProfitState(row):
    dct = {}

    dct['F_transaction_id'] = row[0]
    dct['F_operation_type'] = row[1]
    dct['F_order_id'] = row[2]
    dct['F_agent_id'] = row[3]
    dct['F_merchant_id'] = row[4]
    dct['F_buy_terminal_level'] = row[5]
    dct['F_payment_channel_id'] = row[6]
    dct['F_payment_flag'] = row[7]
    dct['F_merchant_verified_type'] = row[8]
    dct['F_agent_verified_type'] = row[9]
    dct['F_is_bind_sn'] = row[10]
    dct['F_card_type'] = row[11]
    dct['F_agent_type'] = row[12]
    dct['F_profit_rule_type'] = row[13]
    dct['F_pay_type'] = row[14]
    dct['F_profit_type'] = row[15]
    dct['F_flag'] = row[16]
    dct['F_vip_flag'] = row[17]
    dct['F_activity_index'] = row[18]
    dct['F_scheme'] = row[19]
    dct['F_preferential_type'] = row[20]
    dct['F_max_flag'] = row[21]
    dct['F_weixin_activity_flag'] = row[22]
    dct['F_merchant_commission_bymillion'] = row[23]
    dct['F_merchant_max_fee_bymillion'] = row[24]
    dct['F_t0_merchant_fixed_fee_bymillion'] = row[25]
    dct['F_t1_merchant_fixed_fee_bymillion'] = row[26]
    dct['F_cost_commission_bymillion'] = row[27]
    dct['F_cost_max_fee_bymillion'] = row[28]
    dct['F_t0_cost_fixed_fee_bymillion'] = row[29]
    dct['F_t1_cost_fixed_fee_bymillion'] = row[30]
    dct['F_extra_fee_rate_bymillion'] = row[31]
    dct['F_ins_amount_bymillion'] = row[32]
    dct['F_payment_source_type'] = row[33]
    dct['F_amount'] = row[34]
    dct['F_merchant_fee_bymillion'] = row[35]
    dct['F_channel_fee_bymillion'] = row[36]
    dct['F_cost_fee_bymillion'] = row[37]
    dct['F_profit_bymillion'] = row[38]
    dct['F_profit_leshua_bymillion'] = row[39]
    dct['F_time'] = row[40]
    dct['F_create_time'] = row[41]
    dct['F_remark'] = row[42]

    dct['F_profit_bymillion'] = int(dct['F_profit_bymillion'])
    print("old profit bymillion {}".format(row[38]))

    fee = getFee(dct['F_transaction_id'])
    if fee is None:
        return None
    dct['F_payment_flag'] = row[7]
    dct['F_merchant_fee_bymillion'] = int(row[35])
    dct['F_cost_fee_bymillion'] = int(row[37])

    dct['F_new_merchant_fee_bymillion'] = fee * 100
    dct['F_new_profit_bymillion'] = dct['F_new_merchant_fee_bymillion'] - \
                                    dct['F_cost_fee_bymillion']
    #dct['F_source_table'] = sourceTable.getKey(dct['F_transaction_id'])
    return dct


@click.command()
def filterStatement(start, end):
    process = DifferProfit.makeDiffer(makeProfitEmptyDict, loadProfitState)
    process(start, end)


@click.group()
def cli():
    pass








class RedisSet():
    def __init__(self, name):
        self.name = name

    def add(self, key):
        client.sadd(self.name, key)

tranSet = RedisSet('mysset')
agentSet = RedisSet('AgentSet')


@click.command()
def loadTransFlag(start, end):
    for i in range(0, 100):
        sql = ("select F_transaction_id "
               "from t_merchant_statement_{suffix} "
               "where (F_flag&(1<<3)) and F_time <='{end}' "
               "and F_time >='{start}'").format(
                   start=start, end=end, suffix=str(i).zfill(2))
        with SqlManger.Execute(sql) as result:
            for row in result:
                tranSet.add(row[0])

@click.command()
def loadAgentClass():
    sql = "select F_agent_id from lepos.t_agent_info where F_agent_class='11'"
    with SqlManager.Execute(sql) as result:
        for row in result.result:
            agentSet.add(row[0])



cli.add_command(loadAgentClass)
cli.add_command(loadTransFlag)

if __name__ == '__main__':
    cli()
