import sys
import click

import MySQLdb as dbapi
import sys
import csv

dbServer = 'localhost'
dbPass = 'supersecretpassword'
dbSchema = 'dbTest'
dbUser = 'root'


def writeResultRow(sql, fileName):
    db = dbapi.connect(host=dbServer, user=dbUser, passwd=dbPass, db=dbSchema)
    cur = db.cursor()
    cur.execute(sql)
    result = cur.fetchall()
    c = csv.writer(open(fileName, "wb"))

    for row in result:
        c.writerow(row)


def tableName(directory, table):
    return directory + '/' + table + ".csv"


def generateSql(date):
    #F_time>='2018-09-26 00:00:00' and F_time <= '2018-9-26 23:59:59'
    dateStart = date + ' 00:00:00'
    dateEnd = date + ' 23:59:59'

    tables = []

    for i in range(0, 100):
        table_index = "%02d" % int(i)
        table = "t_percent_sub_agent_profit_statement_%s" % (table_index)
        tables.append(table)

        writeResultRow(
            "select * from {table} " + "where F_time>='{dateStart}' " +
            "and F_time <= '{dateEnd}';".format(
                table=table, dateStart=dateStart, dateEnd=dateEnd),
            tableName(table))

    writeResultRow(
        "select * from t_shuabao_agent_bill_detail_daily " +
        "where  F_time>='{dateStart}' and F_time <= '{dateEnd}' ;\n".format(
            dateStart=dateStart, dateEnd=dateEnd),
        tableName('t_shuabao_agent_bill_detail_daily'))


def generateTopSql(directory, date):
    dateStart = date + ' 00:00:00'
    dateEnd = date + ' 23:59:59'

    for i in range(0, 100):
        table_index = "%02d" % int(i)
        table = "t_agent_profit_statement_%s" % (table_index)

        writeResultRow(
            "select * from {table} where F_time>='{dateStart}' and F_time <= '{dateEnd}';\n"
            .format(table=table, dateStart=dateStart, dateEnd=dateEnd),
            tableName(table))


@click.command()
@click.option('--dir', default='', help='scv data store directory')
@click.option('--date', default='', help='format 2019-10-19')
def generateTop(directory, date):
    generateTopSql(directory, date)


@click.group()
def cli():
    pass


def loadItem(row):
    dct = {}
    dct['F_transaction_id'] = row[0]
    dct['F_sub_profit_bill_flag'] = row[9]
    dct['F_amount'] = row[24]
    dct['F_agent_profit_bymillion'] = row[43]
    return dct


#  TABLE `t_percent_sub_agent_profit_statement_86` (
#   `F_transaction_id` varchar(32) NOT NULL DEFAULT '' COMMENT '交易id', 0
#   `F_operation_type` int(10) NOT NULL DEFAULT '0' COMMENT '流水类型:0--t1进账,1--t1出账,2--t0退票',1
#   `F_order_id` varchar(32) NOT NULL COMMENT '交易订单号',2
#   `F_top_agent_id` varchar(32) DEFAULT '0' COMMENT '顶代代理商编号', 3
#   `F_parent_agent_id` varchar(32) DEFAULT '' COMMENT '上级代理商编号', 4
#   `F_lower_agent_id` varchar(32) DEFAULT '0' COMMENT '下级代理商编号', 5
#   `F_agent_id` varchar(32) NOT NULL DEFAULT '0' COMMENT '当前代理商编号', 6
#   `F_agent_type` int(11) DEFAULT '0' COMMENT '服务商类型:1--代理商,2--渠道商,3--服务商', 7
#   `F_agent_level` int(10) unsigned DEFAULT '1' COMMENT '当前代理商级别',8
#   `F_sub_profit_bill_flag` tinyint(1) DEFAULT '1' COMMENT '子代结算：0-否 1-是', 9
#   `F_merchant_id` varchar(20) NOT NULL COMMENT '商户ID', 10
#   `F_agent_verified_type` int(10) NOT NULL DEFAULT '0' COMMENT '审核通过时间:0--20161209 00:00:00 之前，1--20161209 00:00:00 之后', 11
#   `F_merchant_verified_type` int(10) NOT NULL DEFAULT '0' COMMENT '审核通过时间:0--20160905 00:00:00 之前，1--20160905 00:00:00到20161209 00:00:00之间, 2--20161209 00:00:00之后', 12
#   `F_pay_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '1--pos,2-weixin,3-alipay,4-实名支付,5快捷支付', 13
#   `F_payment_flag` tinyint(1) DEFAULT '1' COMMENT '打款方式:0--t0打款,1--t1打款', 14
#   `F_scheme` int(10) NOT NULL DEFAULT '1' COMMENT 't0方案:1--方案1，1--方案2，3--方案3',15
#   `F_card_type` int(10) NOT NULL DEFAULT '0' COMMENT '卡类型:0-未知，1-借记卡，2-贷记卡，3-境外卡', 16
#   `F_preferential_type` tinyint(4) DEFAULT '0' COMMENT '优惠类型：0:标准类，1：优惠类，2：减免类，3：特殊计费类', 17
#   `F_max_flag` tinyint(1) DEFAULT '0' COMMENT '封顶标志:0--非封顶,1--封顶', 18
#   `F_vip_flag` tinyint(3) unsigned DEFAULT '0' COMMENT '0-普通交易,1-刷宝VIP', 19
#   `F_is_bind_sn` tinyint(3) DEFAULT '0' COMMENT '是否绑码:0否1是', 20
#   `F_weixin_activity_flag` int(11) NOT NULL DEFAULT '0' COMMENT '微信活动标志，0--非活动；1--活动', 21
#   `F_payment_source_type` int(11) DEFAULT '0' COMMENT '支付来源：0-未知，1-大POS，2-商务版APP，3-微信公众号，不包括APP和POS的二维码扫码，4-支付宝，不包括APP和POS的二维码扫码', 22
#   `F_activity_index` int(10) NOT NULL DEFAULT '0' COMMENT 't0活动编号 0--非活动类型,1--50秒到，', 23
#   `F_amount` bigint(20) DEFAULT '0' COMMENT '交易额，单位分', 24
#   `F_lower_commission_bymillion` int(10) unsigned DEFAULT '0' COMMENT '下级代理商费率,百万分之一', 25
#   `F_lower_max_fee_bymillion` bigint(10) unsigned DEFAULT '0' COMMENT '下级代理商封顶成本费,百万分之一分', 26
#   `F_t0_lower_fixed_fee_bymillion` bigint(10) unsigned DEFAULT '0' COMMENT '下级代理商t0固定提现成本,百万分之一分', 27
#   `F_t1_lower_fixed_fee_bymillion` bigint(10) unsigned DEFAULT '0' COMMENT '下级代理商t1固定提现成本,百万分之一分', 28
#   `F_lower_extra_fee_rate_bymillion` bigint(20) NOT NULL DEFAULT '0' COMMENT '代理商成本上浮费率,单位:百万分之一', 29
#   `F_lower_cost_fee_bymillion` bigint(20) DEFAULT '0' COMMENT '下级代理商的成本,百万分之一分', 30
#   `F_lower_tran_profit_bymillion` bigint(20) DEFAULT '0' COMMENT '下级代理商交易分成=下级代理商的成本*下级代理商分成比例', 31
#   `F_lower_divide_cent` int(10) DEFAULT '100' COMMENT '下级代理商分成比例，百分之一', 32
#   `F_cost_commission_bymillion` bigint(10) unsigned DEFAULT '0' COMMENT '当前代理商成本费率,百万分之一', 33
#   `F_cost_max_fee_bymillion` bigint(10) unsigned DEFAULT '0' COMMENT '当前代理商封顶成本费,百万分之一分', 34
#   `F_t0_cost_fixed_fee_bymillion` bigint(10) unsigned DEFAULT '0' COMMENT '当前代理商t0固定提现成本,百万分之一分', 35
#   `F_t1_cost_fixed_fee_bymillion` bigint(10) unsigned DEFAULT '0' COMMENT '当前代理商t1固定提现成本,百万分之一分', 36
#   `F_extra_fee_rate_bymillion` bigint(20) NOT NULL DEFAULT '0' COMMENT '当前代理商成本上浮费率,单位:百万分之一', 37
#   `F_ins_amount_bymillion` bigint(20) DEFAULT '0' COMMENT '保险费，单位：百万分之一分', 38
#   `F_cost_fee_bymillion` bigint(20) DEFAULT '0' COMMENT '当前代理商成本,百万分之一分', 39
#   `F_tran_profit_bymillion` bigint(20) DEFAULT '0' COMMENT '当前代理商交易分成=当前代理商成本*当前代理商分成比例', 40
#   `F_divide_cent` int(10) DEFAULT '0' COMMENT '当前代理商分成比例', 41
#   `F_profit_bymillion` bigint(20) DEFAULT '0' COMMENT '代理商总分润(包含子代),百万分之一分', 42
#   `F_agent_profit_bymillion` bigint(20) DEFAULT '0' COMMENT '当前代理商分成=当前代理商交易分成-下级代理商交易分成', 43
#   `F_self_profit_bymillion` bigint(20) DEFAULT '0' COMMENT '当前代理商汇总分成=当前代理商分成+预留下级代理商总分成', 44
#   `F_lower_profit_bymillion` bigint(20) DEFAULT '0' COMMENT '预留下级代理商总分成', 45
#   `F_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '交易时间', 46
#   `F_create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, 47
#   `F_order_filter_flag` tinyint(3) DEFAULT '0' COMMENT '0:未优化 1:优化',
#   PRIMARY KEY (`F_transaction_id`,`F_operation_type`,`F_agent_id`),
#   KEY `K_time` (`F_time`),
#   KEY `K_agent` (`F_order_id`),
#   KEY `I_agent_time` (`F_agent_id`,`F_time`),
#   KEY `I_top_agent` (`F_top_agent_id`)
# ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='成本比例规则代理商子代分润明细'

cli.add_command(generateTop)
if __name__ == '__main__':
    cli()
