#!/usr/bin/python
# -*- coding: utf-8 -*-
#===========================================
#
# Author:      <abel@yeahka.com>
# Create:      2017-08-02 16:04:43
# Name:        create_rerun_sql.py
# Description: 代理商分润重跑需要清除一些数据，这里生成对应的sql脚本
#
#===========================================
import traceback
import sys
import MySQLdb
import datetime

#删除的目标数据库地址
'''
PROFIT_LESHUA_SRC_CFG={
'host':'192.168.50.51',
'user':'selectuser',
'passwd':'www.yeahka.com',
'port':3306,
'charset':'utf8',
'db':'posbill'
}
'''
PROFIT_LESHUA_SRC_CFG = {
    'host': '10.8.18.102',
    'user': 'root',
    'passwd': '1234',
    'port': 3306,
    'charset': 'utf8',
    'db': 'posbill'
}

####数据库错误码
IE_MYSQL_ERROR = -1

####内部错误码
##全局错误码
IE_SUCCESS = 0
IE_SYSTEM_ERR = 1

MYSQL_RECORD_EMPTY = 0
PROFIT_RERUN_TYPE = "1"
XIEJI_PROFIT_RERUN_TYPE = "2"
XIEJI_SUB_PROFIT_RERUN_TYPE = "3"
PAYBACK_PROFIT_RERUN_TYPE = "4"
PROFIT_MONTH_RERUN_TYPE = "5"
PROFIT_SB_VIP_TYPE = "6"
PROFIT_SB_BACK_FIALTER_TYPE = "7"
PROFIT_XJ_FIALTER_TYPE = "8"
PROFIT_XJ_TAX = "9"
PRE_PROFIT_RERUN_TYPE = "10"
PRE_PROFIT_XJ_TM = "11"
PROFIT_XJ_TM = "12"

PRE_PROFIT_RERUN_TYPE_AGENT = "13"
PROFIT_RERUN_TYPE_AGENT = "14"
# PROFIT_RERUN_TYPE_IPAY="15"

# UNFROZEN_PROFIT_RERUN_TYPE="16"
# PROFIT_XJ_WITHFEE = "17"
##

AGENT_TYPE_SB = "6"
AGENT_TYPE_XJ = "5"
AGENT_TYPE_SAAS = "7"
AGENT_TYPE_COMM = "0"

LOG_TYPE_SB_1 = "10"
LOG_TYPE_SB_2 = "11"

LOG_TYPE_XJ_1 = "13"
LOG_TYPE_XJ_2 = "14"

LOG_TYPE_SAAS_1 = "16"
LOG_TYPE_SAAS_2 = "17"
#LOG_TYPE_COMM_1 = "19"
LOG_TYPE_COMM_2 = "20"

LOG_TYPE_SB_UNFROZEN_SETTLE = "22"
LOG_TYPE_COMM_UNFROZEN_SETTLE = "26"


class mysql_db(object):
    def __init__(self, args):
        try:
            self.conn = MySQLdb.connect(**args)
            self.cur = self.conn.cursor()
        except Exception as e:
            raise "mysql connect error:%s" % e.what
            # print 'connect to the db success'

    def execute_sql(self, sql):
        try:
            row_num = self.cur.execute(sql)
            self.conn.commit()
            # xjlog.xieji_log_trace("mysql--%s" % sql)
            return row_num
        except Exception as e:
            xjlog.xieji_log_error("mysql exception--%s,sql:%s" % (str(e), sql))
            return IE_MYSQL_ERROR

    def query(self, sql):
        try:
            row_num = self.cur.execute(sql)
            results = self.cur.fetchall()
            return row_num, results
        except Exception as e:
            xjlog.xieji_log_error("mysql exception--%s,sql:%s" % (str(e), sql))
            return IE_MYSQL_ERROR, ""

    def disconnect(self):
        self.cur.close()
        self.conn.close()


def clear_csv(sqlFile):
    res_file_w = open(sqlFile, "w")  # r只读，w可写，a追加
    res_file_w.close()


def write_csv(sqlFile, content):
    w_content = content + "\n"
    res_file_w = open(sqlFile, "a")  # r只读，w可写，a追加

    res_file_w.write(w_content)
    res_file_w.close()


#如果当天有入账的账单则不能自动生成脚本,需要手动检查
def check_rerun_condition(g_posbill_conn,
                          clear_type, agent_type,
                          g_int_date,
                          g_end_int_date):

    print(clear_type, '-----')

    if clear_type == PROFIT_RERUN_TYPE:
        sql = "select 1 from posbill.t_shuabao_agent_profit_order_daily_bill " \
              "where F_date = %d and F_state!=0" % (g_int_date)
        row_num, results = g_posbill_conn.query(sql)
        if row_num != MYSQL_RECORD_EMPTY:
            err_log = PROFIT_LESHUA_SRC_CFG[
                'host'] + " had incount this day,sql:" + sql
            print err_log
            return IE_SYSTEM_ERR

        sql = "select 1 from posbill.t_agent_profit_order_daily_bill " \
              "where F_date = %d and F_state!=0" % (g_int_date)
        row_num, results = g_posbill_conn.query(sql)
        if row_num != MYSQL_RECORD_EMPTY:
            err_log = PROFIT_LESHUA_SRC_CFG[
                'host'] + " had incount this day,sql:" + sql
            print err_log
            return IE_SYSTEM_ERR
    elif clear_type == PRE_PROFIT_RERUN_TYPE:
        sql = "select 1 from posbill.t_shuabao_agent_profit_order_daily_bill " \
              "where F_date = %d and F_state!=0" % (g_int_date)
        row_num, results = g_posbill_conn.query(sql)
        if row_num != MYSQL_RECORD_EMPTY:
            err_log = PROFIT_LESHUA_SRC_CFG[
                'host'] + " had incount this day,sql:" + sql
            print err_log
            return IE_SYSTEM_ERR

        sql = "select 1 from posbill.t_agent_profit_order_daily_bill " \
              "where F_date = %d and F_state!=0" % (g_int_date)
        row_num, results = g_posbill_conn.query(sql)
        if row_num != MYSQL_RECORD_EMPTY:
            err_log = PROFIT_LESHUA_SRC_CFG[
                'host'] + " had incount this day,sql:" + sql
            print err_log
            return IE_SYSTEM_ERR

    elif clear_type == XIEJI_PROFIT_RERUN_TYPE:
        sql = "select 1 from posbill.t_xieji_agent_profit_order_daily_bill " \
              "where F_date = %d and F_state!=0" % (g_int_date)
        row_num, results = g_posbill_conn.query(sql)
        if row_num != MYSQL_RECORD_EMPTY:
            err_log = PROFIT_LESHUA_SRC_CFG[
                'host'] + " had incount this day,sql:" + sql
            print err_log
            return IE_SYSTEM_ERR
    elif clear_type == XIEJI_SUB_PROFIT_RERUN_TYPE:
        sql = "select 1 from posbill.t_xieji_agent_profit_order_daily_bill " \
              "where F_date = %d and F_state!=0" % (g_int_date)
        row_num, results = g_posbill_conn.query(sql)
        if row_num != MYSQL_RECORD_EMPTY:
            err_log = PROFIT_LESHUA_SRC_CFG[
                'host'] + " had incount this day,sql:" + sql
            print err_log
            return IE_SYSTEM_ERR
    elif clear_type == PAYBACK_PROFIT_RERUN_TYPE:
        sql = "select 1 from posbill.t_payback_agent_profit_order_daily_bill " \
              "where F_date = %d and F_state!=0" % (g_int_date)
        row_num, results = g_posbill_conn.query(sql)
        if row_num != MYSQL_RECORD_EMPTY:
            err_log = PROFIT_LESHUA_SRC_CFG[
                'host'] + " had incount this day,sql:" + sql
            print err_log
            return IE_SYSTEM_ERR

    elif clear_type == PROFIT_MONTH_RERUN_TYPE:
        sql = "select 1 from posbill.t_agent_profit_order_monthly_bill  " \
              "where F_month = '%s' and F_state!=0" % (g_month)
        row_num, results = g_posbill_conn.query(sql)
        if row_num != MYSQL_RECORD_EMPTY:
            err_log = PROFIT_LESHUA_SRC_CFG[
                'host'] + " had incount this day,sql:" + sql
            print err_log
            return IE_SYSTEM_ERR

    elif clear_type == PROFIT_SB_VIP_TYPE:
        sql = "select 1 from posbill.t_agent_terminal_back_money_bill  " \
              "where F_date = %d and F_bill_type in (3,4) and F_state!=0" % (g_int_date)
        row_num, results = g_posbill_conn.query(sql)
        if row_num != MYSQL_RECORD_EMPTY:
            err_log = PROFIT_LESHUA_SRC_CFG[
                'host'] + " had incount this day,sql:" + sql
            print err_log
            return IE_SYSTEM_ERR
    elif clear_type == PROFIT_SB_BACK_FIALTER_TYPE:
        sql = "select 1 from posbill.t_agent_terminal_back_money_bill  " \
              "where F_date = %d and F_bill_type in (1,2) and F_state!=0" % (g_int_date)
        row_num, results = g_posbill_conn.query(sql)
        if row_num != MYSQL_RECORD_EMPTY:
            err_log = PROFIT_LESHUA_SRC_CFG[
                'host'] + " had incount this day,sql:" + sql
            print err_log
            return IE_SYSTEM_ERR
    elif clear_type == PROFIT_XJ_FIALTER_TYPE:
        sql = "select 1 from posbill.t_xieji_agent_profit_order_daily_bill " \
              "where F_date = %d and F_state!=0" % (g_int_date)
        row_num, results = g_posbill_conn.query(sql)
        if row_num != MYSQL_RECORD_EMPTY:
            err_log = PROFIT_LESHUA_SRC_CFG[
                'host'] + " had incount this day,sql:" + sql
            print err_log
            return IE_SYSTEM_ERR
    elif clear_type == PRE_PROFIT_RERUN_TYPE_AGENT:
        if agent_type == AGENT_TYPE_SB:
            sql = "select 1 from posbill.t_shuabao_agent_profit_order_daily_bill " \
                 "where F_date = %d and F_state!=0" % (g_int_date)
            row_num, results = g_posbill_conn.query(sql)
            if row_num != MYSQL_RECORD_EMPTY:
                err_log = PROFIT_LESHUA_SRC_CFG[
                    'host'] + " had incount this day,sql:" + sql
                print err_log
                return IE_SYSTEM_ERR
        elif agent_type == AGENT_TYPE_XJ or agent_type == AGENT_TYPE_SAAS:
            sql = "select 1 from posbill.t_agent_profit_order_daily_bill " \
                 "where F_date = %d and F_state!=0 and F_agent_type=%s" % (g_int_date,agent_type)
            row_num, results = g_posbill_conn.query(sql)
            if row_num != MYSQL_RECORD_EMPTY:
                err_log = PROFIT_LESHUA_SRC_CFG[
                    'host'] + " had incount this day,sql:" + sql
                print err_log
                return IE_SYSTEM_ERR
        else:
            sql = "select 1 from posbill.t_agent_profit_order_daily_bill " \
                "where F_date = %d and F_state!=0 and F_agent_type not in (5,6,7)" % (g_int_date)
            row_num, results = g_posbill_conn.query(sql)
            if row_num != MYSQL_RECORD_EMPTY:
                err_log = PROFIT_LESHUA_SRC_CFG[
                    'host'] + " had incount this day,sql:" + sql
                print err_log
                return IE_SYSTEM_ERR

    elif clear_type == PROFIT_RERUN_TYPE_AGENT:
        if agent_type == AGENT_TYPE_SB:
            sql = "select 1 from posbill.t_shuabao_agent_profit_order_daily_bill " \
                 "where F_date = %d and F_state!=0" % (g_int_date)
            row_num, results = g_posbill_conn.query(sql)
            if row_num != MYSQL_RECORD_EMPTY:
                err_log = PROFIT_LESHUA_SRC_CFG[
                    'host'] + " had incount this day,sql:" + sql
                print err_log
                return IE_SYSTEM_ERR
        elif agent_type == AGENT_TYPE_XJ or agent_type == AGENT_TYPE_SAAS:
            sql = "select 1 from posbill.t_agent_profit_order_daily_bill " \
                 "where F_date = %d and F_state!=0 and F_agent_type=%s" % (g_int_date,agent_type)
            row_num, results = g_posbill_conn.query(sql)
            if row_num != MYSQL_RECORD_EMPTY:
                err_log = PROFIT_LESHUA_SRC_CFG[
                    'host'] + " had incount this day,sql:" + sql
                print err_log
                return IE_SYSTEM_ERR
        else:
            sql = "select 1 from posbill.t_agent_profit_order_daily_bill " \
                 "where F_date = %d and F_state!=0 and F_agent_type not in (5,6,7)" % (g_int_date)
            row_num, results = g_posbill_conn.query(sql)
            if row_num != MYSQL_RECORD_EMPTY:
                err_log = PROFIT_LESHUA_SRC_CFG[
                    'host'] + " had incount this day,sql:" + sql
                print err_log
                return IE_SYSTEM_ERR

    else:
        print "clear_type is invalid"
        return IE_SYSTEM_ERR

    return IE_SUCCESS





def create_rerun_month_profit_sql(g_month):
    print("start to create create_rerun_month_profit_sql")
    int_month_date = int(g_month + "01")
    int_month = int(g_month)

    #删除退票明细
    write_csv("-- 删除日志明细")
    sql = "delete from posbill.t_agent_profit_log where F_date = date('%d') and F_agent_id in (1) ;" \
          % (int_month_date)
    write_csv(sql)

    write_csv("-- 月结明细")
    sql = "delete from posbill.t_agent_bill_detail_monthly where F_month=%d;" \
          % (int_month)
    write_csv(sql)

    write_csv("-- 月结入账单")
    sql = "delete from posbill.t_agent_profit_order_monthly_bill where F_month=%d and F_state=0;" \
          % (int_month)
    write_csv(sql)

    print("create create_rerun_month_profit_sql complete")
    return




def create_pre_profit_detail_sql_by_agent(sqlFile, agent_type, g_str_date,
                                          g_end_str_date):

    try:
        datetime.datetime.strptime(g_str_date, '%Y-%m-%d')
    except ValueError:
        g_str_date = convFormat(g_str_date)
        g_end_str_date = convFormat(g_end_str_date)

    begin_settle_time = g_str_date + " 00:00:00"
    end_settle_time = g_end_str_date + " 23:59:59"
    settle_month = g_str_date[0:4] + g_str_date[5:7]

    # 删除分成明细
    write_csv(sqlFile, ("-- 删除分成，子代分成明细 "
                        "create by create_pre_profit_detail_sql_by_agent(g_str_date)")
              .format(g_str_date=g_str_date))
    for table in range(0, 100):
        table_suffix = "%02d" % table
        sql = "delete from posbill.t_agent_profit_statement_%s where F_time between '%s' and '%s' %s;" \
              % (table_suffix, begin_settle_time, end_settle_time,agent_type)
        write_csv(sqlFile, sql)

        sql = "delete from posbill.t_sub_agent_profit_statement_%s where F_time between '%s' and '%s' %s;" \
              % (table_suffix, begin_settle_time, end_settle_time,agent_type)
        write_csv(sqlFile, sql)
        sql = "delete from posbill.t_rule_sub_agent_profit_statement_%s where F_time between '%s' and '%s' %s;" \
              % (table_suffix, begin_settle_time, end_settle_time,agent_type)
        write_csv(sqlFile, sql)
        sql = "delete from posbill.t_percent_sub_agent_profit_statement_%s where F_time between '%s' and '%s' %s;" \
              % (table_suffix, begin_settle_time, end_settle_time,agent_type)
        write_csv(sqlFile, sql)
    agent_ids = []
    if agent_type == AGENT_TYPE_XJ:
        agent_ids = ['78860']
    elif agent_type == AGENT_TYPE_SAAS:
        agent_ids = ['3233605', '78702']
    for agentid in agent_ids:
        sql = "delete from posbill.t_agent_profit_statement_%s_%s where F_time between '%s' and '%s';" \
              % (agentid,settle_month, begin_settle_time, end_settle_time)
        write_csv(sqlFile, sql)

        sql = "delete from posbill.t_sub_agent_profit_statement_%s_%s where F_time between '%s' and '%s';" \
              % (agentid,settle_month, begin_settle_time, end_settle_time)
        write_csv(sqlFile, sql)
        sql = "delete from posbill.t_rule_sub_agent_profit_statement_%s_%s where F_time between '%s' and '%s';" \
              % (agentid,settle_month, begin_settle_time, end_settle_time)
        write_csv(sqlFile, sql)
        sql = "delete from posbill.t_percent_sub_agent_profit_statement_%s_%s where F_time between '%s' and '%s';" \
              % (agentid,settle_month, begin_settle_time, end_settle_time)
        write_csv(sqlFile, sql)
    return


def create_pre_rerun_profit_sql_by_agent(
        g_sql_file, g_clear_type, g_agent_type, g_int_date, g_end_int_date):
    print("start to create profit rerun sql", g_clear_type, g_agent_type)
    write_csv(g_sql_file, "-- ")
    write_csv(g_sql_file, "-- 生成代理商分润重跑清除数据的sql脚本")
    write_csv(g_sql_file, "-- ")
    agent_type = "0"
    if g_agent_type == AGENT_TYPE_SB:
        log_type = LOG_TYPE_SB_1
        agent_type = " and F_agent_type in (6,11,14)"
    else:
        print "agent type error (%s)" % (agent_type)
        return

    write_csv(g_sql_file, "-- 1. 重入日志和倒挂明细")
    sql = "delete from posbill.t_agent_profit_log where F_date between '%d' and '%d' and F_agent_id in (%s);" \
          % (g_int_date,g_end_int_date,log_type)
    write_csv(g_sql_file, sql)

    begin_settle_time = formatDate(g_int_date) + " 00:00:00"
    end_settle_time = formatDate(g_end_int_date) + " 23:59:59"
    sql = "delete from posbill.t_agent_profit_statement_daogua where F_time between '%s' and '%s' %s;" \
          % (begin_settle_time, end_settle_time,agent_type)
    write_csv(g_sql_file, sql)
    write_csv(g_sql_file, "-- 2 百表明细分成，子代分成，退票数据清理;退票明细")
    create_pre_profit_detail_sql_by_agent(g_sql_file,
                                          agent_type, formatDate(g_int_date),
                                          formatDate(g_end_int_date))

    write_csv(g_sql_file, "-- 2 清除优化交易")
    sql = "delete from posbill.t_agent_profit_statement_daogua_filter where F_time between '%s' and '%s' %s;" \
             % (begin_settle_time, end_settle_time,agent_type)
    write_csv(g_sql_file, sql)
    sql = "delete from posbill.t_agent_profit_statement_filter where F_time between '%s' and '%s' %s;" \
             % (begin_settle_time, end_settle_time,agent_type)
    write_csv(g_sql_file, sql)

    sql = "delete from posbill.t_sub_agent_profit_statement_filter where F_time between '%s' and '%s' %s;" \
                % (begin_settle_time, end_settle_time,agent_type)
    write_csv(g_sql_file, sql)

    sql = "delete from posbill.t_rule_sub_agent_profit_statement_filter where F_time between '%s' and '%s' %s;" \
                % (begin_settle_time, end_settle_time,agent_type)
    write_csv(g_sql_file, sql)

    sql = "delete from posbill.t_percent_sub_agent_profit_statement_filter where F_time between '%s' and '%s' %s;" \
                % (begin_settle_time, end_settle_time,agent_type)
    write_csv(g_sql_file, sql)
    sql = "delete from posbill.t_agent_profit_filter_order where F_date>='%s' and F_date<='%s' %s;" \
                % (g_int_date, g_end_int_date,agent_type)
    write_csv(g_sql_file, sql)

    print("create profit rerun sql complete")
    return

def formatDate(intDate):
    return convFormat(str(intDate))

def convFormat(date):
    start_date = datetime.datetime.strptime(date, '%Y%m%d')
    dateStr = start_date.strftime('%Y-%m-%d')
    return dateStr



def create_rerun_profit_sql_by_agent(
        g_sql_file, g_clear_type, g_agent_type, g_int_date, g_end_int_date):
    print("start to create profit rerun sql")
    write_csv(g_sql_file, "-- ")
    write_csv(g_sql_file, "-- 生成代理商分润重跑清除数据的sql脚本")
    write_csv(g_sql_file, "-- ")
    agent_type = "0"
    log_type = ""
    if g_agent_type == AGENT_TYPE_XJ:
        log_type = LOG_TYPE_XJ_2
        agent_type = " and F_agent_type=5"
    elif g_agent_type == AGENT_TYPE_SB:
        log_type = LOG_TYPE_SB_2
        agent_type = " and F_agent_type in (6,11,14)"
    elif g_agent_type == AGENT_TYPE_SAAS:
        log_type = LOG_TYPE_SAAS_2
        agent_type = " and F_agent_type=7"
    else:
        print "agent type error (%s)" % (agent_type)
        return

    write_csv(g_sql_file, "-- 1. 重入日志和倒挂明细")
    sql = "delete from posbill.t_agent_profit_log where F_date between '%d' and '%d' and F_agent_id in (%s);" \
          % (g_int_date,g_end_int_date,log_type)
    write_csv(g_sql_file, sql)

    begin_settle_time = formatDate(g_int_date) + " 00:00:00"
    end_settle_time = formatDate(g_end_int_date) + " 23:59:59"
    sql = "delete from posbill.t_agent_profit_statement_daogua where F_exception_type=9 and F_time between '%s' and '%s' %s;" \
          % (begin_settle_time, end_settle_time,agent_type)
    write_csv(g_sql_file, sql)

    sql = "delete from posbill.t_agent_profit_statement_daogua_filter where F_exception_type=9 and F_time between '%s' and '%s' %s;" \
          % (begin_settle_time, end_settle_time,agent_type)
    write_csv(g_sql_file, sql)

    #删除退票明细
    write_csv(g_sql_file, "-- 删除退票明细")
    sql = "delete from posbill.t_agent_profit_statement_t0refund where F_create_time between '%s' and '%s' %s;" \
          % (begin_settle_time, end_settle_time,agent_type)
    write_csv(g_sql_file, sql)

    sql = "delete from posbill.t_agent_profit_statement_t0refund_filter where F_create_time between '%s' and '%s' %s;" \
          % (begin_settle_time, end_settle_time,agent_type)
    write_csv(g_sql_file, sql)

    sql = "delete from posbill.t_sub_agent_profit_statement_t0refund where F_create_time between '%s' and '%s' %s;" \
          % (begin_settle_time, end_settle_time,agent_type)
    write_csv(g_sql_file, sql)

    sql = "delete from posbill.t_sub_agent_profit_statement_t0refund_filter where F_create_time between '%s' and '%s' %s;" \
          % (begin_settle_time, end_settle_time,agent_type)
    write_csv(g_sql_file, sql)

    sql = "delete from posbill.t_rule_sub_agent_profit_statement_t0refund where F_create_time between '%s' and '%s' %s;" \
          % (begin_settle_time, end_settle_time,agent_type)
    write_csv(g_sql_file, sql)

    sql = "delete from posbill.t_rule_sub_agent_profit_statement_t0refund_filter where F_create_time between '%s' and '%s' %s;" \
          % (begin_settle_time, end_settle_time,agent_type)
    write_csv(g_sql_file, sql)

    sql = "delete from posbill.t_percent_sub_agent_profit_statement_t0refund where F_create_time between '%s' and '%s' %s;" % (
        begin_settle_time, end_settle_time, agent_type)
    write_csv(g_sql_file, sql)

    sql = "delete from posbill.t_percent_sub_agent_profit_statement_t0refund_filter where F_create_time between '%s' and '%s' %s;" % (
        begin_settle_time, end_settle_time, agent_type)
    write_csv(g_sql_file, sql)

    if agent_type != AGENT_TYPE_SB:
        write_csv(g_sql_file, "-- 3 非刷宝日汇总和日账单数据")
        write_csv(g_sql_file, "-- a. 日汇总")
        sql = "delete from posbill.t_agent_bill_detail_daily where F_date between '%d' and '%d' %s;" \
          % (g_int_date,g_end_int_date,agent_type)
        write_csv(g_sql_file, sql)

        write_csv(g_sql_file, "-- b.账单,如果已经入账，则不能随便删，要不然重复入账")
        sql = "delete from posbill.t_agent_profit_order_daily_bill where F_date between '%d' and '%d' and F_state=0 %s;" \
            % (g_int_date,g_end_int_date,agent_type)
        write_csv(g_sql_file, sql)
    else:
        write_csv(g_sql_file, "-- 4 刷宝日汇总和日账单数据")
        write_csv(g_sql_file, "-- a. 日汇总")
        sql = "delete from posbill.t_shuabao_agent_bill_detail_daily where F_date between '%d' and '%d';" \
            % (g_int_date,g_end_int_date)
        write_csv(g_sql_file, sql)

        write_csv(g_sql_file, "-- b.账单,如果已经入账，则不能随便删，要不然重复入账")
        sql = "delete from posbill.t_shuabao_agent_profit_order_daily_bill where F_date between '%d' and '%d' and F_state=0;" \
            % (g_int_date,g_end_int_date)
        write_csv(g_sql_file, sql)

    print("create profit rerun sql complete")
    return


def pymain(g_sql_file, g_str_date, g_end_str_date, g_clear_type, g_agent_type):
    print(g_sql_file, g_str_date, g_end_str_date, g_clear_type, g_agent_type)
    clear_csv(g_sql_file)
    g_int_date = int(g_str_date.replace('-', ''))
    g_end_int_date = int(g_end_str_date.replace('-', ''))
    g_posbill_conn = mysql_db(PROFIT_LESHUA_SRC_CFG)
    #全局资源释放

    ret = check_rerun_condition(g_posbill_conn, g_clear_type, g_agent_type,
                                g_int_date, g_end_int_date)
    if ret != IE_SUCCESS:
        print('check failed')
        return ret

    #判断顶代发票是否足够
    elif g_clear_type == PRE_PROFIT_RERUN_TYPE_AGENT:
        create_pre_rerun_profit_sql_by_agent(
            g_sql_file, g_clear_type, g_agent_type, g_int_date, g_end_int_date)
    elif g_clear_type == PROFIT_RERUN_TYPE_AGENT:
        create_rerun_profit_sql_by_agent(
            g_sql_file, g_clear_type, g_agent_type, g_int_date, g_end_int_date)
    else:
        print(
            "usage: cmd 1/2/3/4(profit/xiejieprofit/xiejisubprofit/payback) 2017-05-01 2017-05-01"
        )
        return -1

    g_posbill_conn.disconnect()
    return IE_SUCCESS


if __name__ == '__main__':
    reload(sys)
    sys.setdefaultencoding('utf-8')
    print sys.argv
    begin_date = ""
    end_date = ""
    g_agent_type = "0"
    agent_type = "0"
    #默认前一天
    if len(sys.argv) == 2:
        today = datetime.date.today()
        oneday = datetime.timedelta(days=1)
        yesterday = today - oneday
        begin_date = str(yesterday)
        end_date = str(yesterday)
    elif len(sys.argv) == 4 and len(sys.argv[2]) == 10:
        begin_date = sys.argv[2]
        end_date = sys.argv[3]
    elif len(sys.argv) == 5 and (sys.argv[1] == PRE_PROFIT_RERUN_TYPE_AGENT
                                 or sys.argv[1] == PROFIT_RERUN_TYPE_AGENT):
        begin_date = sys.argv[2]
        end_date = sys.argv[3]
        agent_type = sys.argv[4]
    else:
        print(
            "usage: cmd 1/2/3/4/5/6/7/8/9/10(1profit预结算/2xiejieprofit/3xiejisubprofit/4paybackprofit/"
            "5monthprofit/6sbvip/7sbbackfilter/8xjfilter/9xjtax/10profit清算/11xjtm预结算/12xjtm清算/13新分润预清算/14新分润清算 [5xj/6sb/7saas/0common]/15智能支付/17xjwf) "
            "[2017-05-01 2017-05-02]")
        sys.exit(1)
    print("start to create rerun sql from:%s to:%s" % (begin_date, end_date))
    try:
        #全局变量定义
        g_posbill_conn = mysql_db(PROFIT_LESHUA_SRC_CFG)
        g_sql_file = "create_rerun_sql.sql"
        g_str_date = begin_date
        g_end_str_date = end_date
        g_int_date = int(g_str_date.replace('-', ''))
        g_end_int_date = int(g_end_str_date.replace('-', ''))
        g_clear_type = sys.argv[1]
        g_month = g_str_date.replace('-', '')[0:6]
        g_agent_type = agent_type

        ret = pymain(g_sql_file, g_str_date, g_end_str_date, g_clear_type,
                     g_agent_type)
        if ret != 0:
            print("create rerun sql error")
            sys.exit(1)

    except Exception as e:
        print(traceback.format_exc())
        sys.exit(1)
