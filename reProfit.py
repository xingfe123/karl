import MySQLdb as dbapi
import redis
import sys
import csv
import os
path = os.path.abspath(__file__)
dir_path = os.path.dirname(path)
sys.path.append(dir_path)
import DifferProfit 


# dbServer='10.8.18.102'
# dbPass='1234'
# dbSchema='posbill'
# dbUser='root'

dbServerNew = '10.8.24.98'
dbPassNew = 'www.yeahka.com'
dbSchemaNew = 'posbill'
#mysql -A -uselectuser -pwww.yeahka.com -h10.8.24.84 --default-character-set=utf8 posbill -A
dbUserNew = 'settle_select'

dbUser = 'selectuser'
dbPass = 'www.yeahka.com'
dbServer = '10.8.24.84'
dbSchema = 'posbill'


class redisDict:
    def __init__(self, name):
        self.client = redis.Redis(host='127.0.0.1', port=6379, db=0)
        self.name = name

    def setKey(self, key, value):
        self.client.hset(self.name, key, value)

    def getKey(self, key):
        hashVal = self.client.hget(self.name, key)
        return hashVal


transactionsMerchant = redisDict('trans')
insurances = redisDict('insurance')


def getFee(transId):
    fee = transactionsMerchant.getKey(transId)
    if fee is None:
        return None
    ins = insurances.getKey(transId)

    return int(fee) - int(ins)


def setFee(transaction_id, merchant_commission_amount, insurance_fee_million):
    transactionsMerchant.setKey(transaction_id, merchant_commission_amount)
    insurances.setKey(transaction_id, insurance_fee_million)


#-------------


def agentSubProfitStatementTables():
    return tables('t_percent_sub_agent_profit_statement_')


def merchantStatements():
    return tables("t_merchant_statement_")


def agentProfitStatementTables():
    return tables('t_agent_profit_statement_')


def tables(table_base):
    lst = []
    for i in range(0, 100):
        table_index = "%02d" % int(i)
        lst.append(table_base + table_index)
    return lst


def LoadToProfit(begin_time, end_time):
    for table in merchantStatements():
        print("load " + table)
        loadOneTable(table, begin_time, end_time)

    #print('----', len(transactionsMerchant))


def loadOneTable(table, begin_time, end_time):
    sql =  "select F_transaction_id,F_order_id,F_merchant_id,F_payment_channel_id,F_max_fee_flag," \
    + "F_operation_type,F_amount,F_merchant_commission_amount_bywan," \
    + "F_pay_channel_commission_amount_bywan,F_time,F_agent_id,F_flag,F_trade_card_type," \
    + "F_preferential_type,F_extra_fee_rate,F_payment_source_type,F_sp_id2,F_vip_flag, F_keep_accounts_code," \
    + "ifnull(F_ins_amount_bywan, 0) ,date_format(F_time,'%Y%m%d'),ifnull(F_other_fee_bywan, 0),F_fee_rate " \
    + " from "+table \
    +" where  (F_flag&(1<<3)) AND F_payment_source_type = 13 and F_other_fee_bywan > 0 and F_time>='" \
    + begin_time + "' and F_time<='" + end_time +"'"

    try:
        db = dbapi.connect(
            host=dbServer, user=dbUser, passwd=dbPass, db=dbSchema)
        cur = db.cursor()
        cur.execute(sql)
        result = cur.fetchall()

        print("load table:" + table + ":" + sql)
        for row in result:
            #_transaction_id => F_merchant_commission_amount_bywan
            transaction_id = row[0]  #int(row[0])
            merchant_commission_amount = row[7]  #int(row[7]) * 100
            insurance_fee_million = row[19]  #  * 100;
            setFee(transaction_id, merchant_commission_amount,
                   insurance_fee_million)

            #sourceTable.setKey(transaction_id, table)
            print(table, transaction_id, merchant_commission_amount)

    except Exception as e:
        print('eeeor', e)




def makeProfitEmptyDict():
    dct = {}
    dct['F_transaction_id'] = 0
    dct['F_operation_type'] = 0
    dct['F_order_id'] = 0
    dct['F_agent_id'] = 0
    dct['F_merchant_id'] = 0
    dct['F_buy_terminal_level'] = 0
    dct['F_payment_channel_id'] = 0
    dct['F_payment_flag'] = 0
    dct['F_merchant_verified_type'] = 0
    dct['F_agent_verified_type'] = 0
    dct['F_is_bind_sn'] = 0
    dct['F_card_type'] = 0
    dct['F_agent_type'] = 0
    dct['F_profit_rule_type'] = 0
    dct['F_pay_type'] = 0
    dct['F_profit_type'] = 0
    dct['F_flag'] = 0
    dct['F_vip_flag'] = 0
    dct['F_activity_index'] = 0
    dct['F_scheme'] = 0
    dct['F_preferential_type'] = 0
    dct['F_max_flag'] = 0
    dct['F_weixin_activity_flag'] = 0
    dct['F_merchant_commission_bymillion'] = 0
    dct['F_merchant_max_fee_bymillion'] = 0
    dct['F_t0_merchant_fixed_fee_bymillion'] = 0
    dct['F_t1_merchant_fixed_fee_bymillion'] = 0
    dct['F_cost_commission_bymillion'] = 0
    dct['F_cost_max_fee_bymillion'] = 0
    dct['F_t0_cost_fixed_fee_bymillion'] = 0
    dct['F_t1_cost_fixed_fee_bymillion'] = 0
    dct['F_extra_fee_rate_bymillion'] = 0
    dct['F_ins_amount_bymillion'] = 0
    dct['F_payment_source_type'] = 0
    dct['F_amount'] = 0
    dct['F_merchant_fee_bymillion'] = 0
    dct['F_channel_fee_bymillion'] = 0
    dct['F_cost_fee_bymillion'] = 0
    dct['F_profit_bymillion'] = 0
    dct['F_profit_leshua_bymillion'] = 0
    dct['F_time'] = 0
    dct['F_create_time'] = 0
    dct['F_remark'] = 0

    dct['F_new_merchant_fee_bymillion'] = 0
    dct['F_new_profit_bymillion'] = 0

    return dct


def loadProfitState(row):
    dct = {}

    dct['F_transaction_id'] = row[0]
    dct['F_operation_type'] = row[1]
    dct['F_order_id'] = row[2]
    dct['F_agent_id'] = row[3]
    dct['F_merchant_id'] = row[4]
    dct['F_buy_terminal_level'] = row[5]
    dct['F_payment_channel_id'] = row[6]
    dct['F_payment_flag'] = row[7]
    dct['F_merchant_verified_type'] = row[8]
    dct['F_agent_verified_type'] = row[9]
    dct['F_is_bind_sn'] = row[10]
    dct['F_card_type'] = row[11]
    dct['F_agent_type'] = row[12]
    dct['F_profit_rule_type'] = row[13]
    dct['F_pay_type'] = row[14]
    dct['F_profit_type'] = row[15]
    dct['F_flag'] = row[16]
    dct['F_vip_flag'] = row[17]
    dct['F_activity_index'] = row[18]
    dct['F_scheme'] = row[19]
    dct['F_preferential_type'] = row[20]
    dct['F_max_flag'] = row[21]
    dct['F_weixin_activity_flag'] = row[22]
    dct['F_merchant_commission_bymillion'] = row[23]
    dct['F_merchant_max_fee_bymillion'] = row[24]
    dct['F_t0_merchant_fixed_fee_bymillion'] = row[25]
    dct['F_t1_merchant_fixed_fee_bymillion'] = row[26]
    dct['F_cost_commission_bymillion'] = row[27]
    dct['F_cost_max_fee_bymillion'] = row[28]
    dct['F_t0_cost_fixed_fee_bymillion'] = row[29]
    dct['F_t1_cost_fixed_fee_bymillion'] = row[30]
    dct['F_extra_fee_rate_bymillion'] = row[31]
    dct['F_ins_amount_bymillion'] = row[32]
    dct['F_payment_source_type'] = row[33]
    dct['F_amount'] = row[34]
    dct['F_merchant_fee_bymillion'] = row[35]
    dct['F_channel_fee_bymillion'] = row[36]
    dct['F_cost_fee_bymillion'] = row[37]
    dct['F_profit_bymillion'] = row[38]
    dct['F_profit_leshua_bymillion'] = row[39]
    dct['F_time'] = row[40]
    dct['F_create_time'] = row[41]
    dct['F_remark'] = row[42]

    dct['F_profit_bymillion'] = int(dct['F_profit_bymillion'])
    print("old profit bymillion {}".format(row[38]))

    fee = getFee(dct['F_transaction_id'])
    if fee is None:
        return None
    dct['F_payment_flag'] = row[7]
    dct['F_merchant_fee_bymillion'] = int(row[35])
    dct['F_cost_fee_bymillion'] = int(row[37])

    dct['F_new_merchant_fee_bymillion'] = fee * 100
    dct['F_new_profit_bymillion'] = dct['F_new_merchant_fee_bymillion'] - \
                                    dct['F_cost_fee_bymillion']
    #dct['F_source_table'] = sourceTable.getKey(dct['F_transaction_id'])
    return dct


def makeSubProfitEmptyDict():
    dct = {}
    dct['F_transaction_id'] = 0
    dct['F_operation_type'] = 0
    dct['F_order_id'] = 0
    dct['F_top_agent_id'] = 0
    dct['F_parent_agent_id'] = 0
    dct['F_lower_agent_id'] = 0
    dct['F_agent_id'] = 0
    dct['F_agent_type'] = 0
    dct['F_agent_level'] = 0
    dct['F_sub_profit_bill_flag'] = 0
    dct['F_merchant_id'] = 0
    dct['F_agent_verified_type'] = 0
    dct['F_merchant_verified_type'] = 0
    dct['F_pay_type'] = 0
    dct['F_payment_flag'] = 0
    dct['F_scheme'] = 0
    dct['F_card_type'] = 0
    dct['F_preferential_type'] = 0
    dct['F_max_flag'] = 0
    dct['F_vip_flag'] = 0
    dct['F_is_bind_sn'] = 0
    dct['F_weixin_activity_flag'] = 0
    dct['F_payment_source_type'] = 0
    dct['F_activity_index'] = 0
    dct['F_amount'] = 0
    dct['F_lower_commission_bymillion'] = 0
    dct['F_lower_max_fee_bymillion'] = 0
    dct['F_t0_lower_fixed_fee_bymillion'] = 0
    dct['F_t1_lower_fixed_fee_bymillion'] = 0
    dct['F_lower_extra_fee_rate_bymillion'] = 0
    dct['F_lower_cost_fee_bymillion'] = 0
    dct['F_lower_tran_profit_bymillion'] = 0
    dct['F_lower_divide_cent'] = 0
    dct['F_cost_commission_bymillion'] = 0
    dct['F_cost_max_fee_bymillion'] = 0
    dct['F_t0_cost_fixed_fee_bymillion'] = 0
    dct['F_t1_cost_fixed_fee_bymillion'] = 0
    dct['F_extra_fee_rate_bymillion'] = 0
    dct['F_ins_amount_bymillion'] = 0
    dct['F_cost_fee_bymillion'] = 0
    dct['F_tran_profit_bymillion'] = 0
    dct['F_divide_cent'] = 0
    dct['F_profit_bymillion'] = 0
    dct['F_agent_profit_bymillion'] = 0
    dct['F_self_profit_bymillion'] = 0
    dct['F_lower_profit_bymillion'] = 0
    dct['F_time'] = 0
    dct['F_create_time'] = 0

    dct['F_new_merchant_fee_bymillion'] = 0
    dct['F_new_profit_bymillion'] = 0

    return dct


def loadPercentSubAgentProfitStatement(row):
    dct = {}
    dct['F_transaction_id'] = row[0]
    dct['F_operation_type'] = row[1]
    dct['F_order_id'] = row[2]
    dct['F_top_agent_id'] = row[3]
    dct['F_parent_agent_id'] = row[4]
    dct['F_lower_agent_id'] = row[5]
    dct['F_agent_id'] = row[6]
    dct['F_agent_type'] = row[7]
    dct['F_agent_level'] = row[8]
    dct['F_sub_profit_bill_flag'] = row[9]
    dct['F_merchant_id'] = row[10]
    dct['F_agent_verified_type'] = row[11]
    dct['F_merchant_verified_type'] = row[12]
    dct['F_pay_type'] = row[13]
    dct['F_payment_flag'] = row[14]
    dct['F_scheme'] = row[15]
    dct['F_card_type'] = row[16]
    dct['F_preferential_type'] = row[17]
    dct['F_max_flag'] = row[18]
    dct['F_vip_flag'] = row[19]
    dct['F_is_bind_sn'] = row[20]
    dct['F_weixin_activity_flag'] = row[21]
    dct['F_payment_source_type'] = row[22]
    dct['F_activity_index'] = row[23]
    dct['F_amount'] = row[24]
    dct['F_lower_commission_bymillion'] = row[25]
    dct['F_lower_max_fee_bymillion'] = row[26]
    dct['F_t0_lower_fixed_fee_bymillion'] = row[27]
    dct['F_t1_lower_fixed_fee_bymillion'] = row[28]
    dct['F_lower_extra_fee_rate_bymillion'] = row[29]
    dct['F_lower_cost_fee_bymillion'] = row[30]
    dct['F_lower_tran_profit_bymillion'] = row[31]
    dct['F_lower_divide_cent'] = row[32]
    dct['F_cost_commission_bymillion'] = row[33]
    dct['F_cost_max_fee_bymillion'] = row[34]
    dct['F_t0_cost_fixed_fee_bymillion'] = row[35]
    dct['F_t1_cost_fixed_fee_bymillion'] = row[36]
    dct['F_extra_fee_rate_bymillion'] = row[37]
    dct['F_ins_amount_bymillion'] = row[38]
    dct['F_cost_fee_bymillion'] = row[39]
    dct['F_tran_profit_bymillion'] = row[40]
    dct['F_divide_cent'] = row[41]
    dct['F_profit_bymillion'] = row[42]
    dct['F_agent_profit_bymillion'] = row[43]
    dct['F_self_profit_bymillion'] = row[44]
    dct['F_lower_profit_bymillion'] = row[45]
    dct['F_time'] = row[46]
    dct['F_create_time'] = row[47]

    fee = getFee(dct['F_transaction_id'])
    if fee is None:
        return None

    dct['F_lower_cost_fee_bymillion'] = int(row[30])
    dct['F_lower_divide_cent'] = int(row[32])
    dct['F_cost_fee_bymillion'] = int(row[39])
    dct['F_profit_bymillion'] = int(row[43])
    dct['F_agent_profit_bymillion'] = int(row[44])

    dct['F_new_merchant_fee_bymillion'] = fee * 100
    dct['F_new_profit_bymillion'] = (fee*100- dct['F_cost_fee_bymillion']) * dct['F_divide_cent'] \
        - (fee*100 - dct['F_lower_cost_fee_bymillion']) * dct['F_lower_divide_cent']

    return dct


def diffProfitStateTo(writer, table, start, end):
    sql = "select * from {table} where F_payment_flag = 1 and F_agent_type = 6 and F_time >= '{start}' and F_time <='{end}';".format(
        table=table, start=start, end=end)

    db = dbapi.connect(
        host=dbServerNew, user=dbUserNew, passwd=dbPassNew, db=dbSchemaNew)
    cur = db.cursor()
    cur.execute(sql)
    result = cur.fetchall()

    for row in result:
        profitState = loadProfitState(row)
        if profitState is None:
            continue

        writer.writeRow(profitState.values())
        print(table, profitState)


def tables(table_base):
    lst = []
    for i in range(0, 100):
        table_index = "%02d" % int(i)
        lst.append(table_base + table_index)
    return lst


def diffSubProfitStateTo(writer, table, start, end):
    #t_percent_sub_agent_profit_statement_
    sql = "select * from {table} where F_payment_flag = 1 and F_agent_type = 6 and F_time >= '{start}' and F_time <='{end}';".format(
        table=table, start=start, end=end)

    db = dbapi.connect(
        host=dbServerNew, user=dbUserNew, passwd=dbPassNew, db=dbSchemaNew)
    cur = db.cursor()
    cur.execute(sql)
    result = cur.fetchall()

    for row in result:
        profitState = loadPercentSubAgentProfitStatement(row)
        if profitState is None:
            continue

        writer.writeRow(profitState.values())
        print(table, profitState)



    
if __name__ == '__main__':

    end = '2018-10-11 23:59:59'
    start = '2018-09-25 00:00:00'
    if len(sys.argv) >= 2 and sys.argv[1] == "load":
        LoadToProfit(start, end)
        exit(0)

    if len(sys.argv) >= 2 and sys.argv[1] == "subProfit":
        writer = DiffWriter('tmpdata/t_percent_sub_agent_profit_statement.csv')
        writer.writeHead(makeSubProfitEmptyDict())
        for table in agentSubProfitStatementTables():
            diffSubProfitStateTo(writer, table, start, end)
        writer.close()
        exit(0)

    try:
        process = DifferProfit.makeDiffer(makeProfitEmptyDict, diffProfitStateTo)
        process(start, end)
        
        exit(0)
    except Exception as e:
        print(e, "hello")

    # sql2 = "select * from t_percent_sub_agent_profit_statement_00 limit 1;"
    # cur2 = db.cursor()
    # cur2.execute(sql2)

    # for row in cur2.fetchall():
    #     # print(row)
    #     percent = loadPercentSubAgentProfitStatement(row, db)
    #     print("percent_sub_agent_profit_statement", percent)
