#!/usr/bin/python
# -*- coding: utf-8 -*-
import MySQLdb as dbapi

dbServer = '10.8.18.102'
dbPass = '1234'
dbSchema = 'posbill'
dbUser = 'root'


def actionTable(i):
    db = dbapi.connect(host=dbServer, user=dbUser, passwd=dbPass, db=dbSchema)
    cur = db.cursor()

    sql = ("select F_amount, from t_merchant_statement_{index} "
           "where F_agent_id='11768' and"
           " F_time >= '2018-09-25 00:00:00' and  "
           " F_time <= '2018-10-11 23:59:59' and"
           " (F_flag&(1<<3)) AND F_payment_source_type = 13 "
           " and F_other_fee_bywan > 0 ").format(index=str(i).zfill(2))

    #print(sql)
    cur.execute(sql)
    result = cur.fetchall()
    transId = []
    for row in result:
        assert (len(row) >= 2)
        print(row)
        transId.append(row[1])
    return transId


def querySum(rows):
    ids = ["'tranId'".format(tranId=row[1]) for row in rows]
    tranId = " , ".join(ids)
    sql = ("select sum(F_amount) from t_agent_profit_statement_68 "
           "where F_agent_id='11768' and "
           " F_transaction_id in ({tranIds}) and "
           " F_payment_flag = 1 and F_agent_type = 6 and"
           " F_payment_source_type = 13 and "
           " F_time >= '2018-09-25 00:00:00' and  "
           " F_time <= '2018-10-11 23:59:59'").format(tranId=tranId)

    db = dbapi.connect(host=dbServer, user=dbUser, passwd=dbPass, db=dbSchema)
    cur = db.cursor()

    print(sql)
    cur.execute(sql)
    result = cur.fetchall()
    for row in result:
        print(row)


if __name__ == '__main__':
    transId = []
    for i in range(0, 100):
        transId.extend(actionTable(i))
