#coding:utf-8
import csv


def parserFile(parser, fileName):
    line = 0
    totalLine = 0
    with open(fileName, 'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter='\t', quotechar='|')
        for row in spamreader:
            totalLine += 1
            if not parser(row):
                line += 1

    print("{} totalline {} , checkfailed {} line".format(
        fileName, totalLine, line))


def findError(parser, fileName):
    line = 0
    totalLine = 0
    lst = []
    with open(fileName, 'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter='\t', quotechar='|')
        for row in spamreader:
            totalLine += 1
            if not parser(row):
                transactionId = int(row[0])
                lst.append(transactionId)

    print("{} totalline {} , checkfailed {} line".format(
        fileName, totalLine, line))
    return set(lst)


def loadCvsFiles(partner_profit, tiaoma, partner_payback_profit, errorFile):
    errors = loadError(errorFile)
    parserFile(newParsePartnerProfitWithError(errors), partner_profit)
    parserFile(newParseTiaoma(errors), tiaoma)
    parserFile(parsePartnerPaybackProfit, partner_payback_profit)

    otherError = findError(parsePartnerProfit, partner_profit)
    for transactionId in otherError:
        print(transactionId)

    ok = errors - otherError
    print('-------------', len(ok))
    for transactionId in errors - otherError:
        print(transactionId)


def loadError(errorFile):
    line = 0
    totalLine = 0
    lst = []
    with open(errorFile, 'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter='\t', quotechar='|')
        for row in spamreader:
            totalLine += 1
            lst.append(int(row[0]))

    print("{} totalline {} , load failed {} line".format(
        errorFile, totalLine, line))
    for line in lst:
        print("error trans id is {}".format(line))

    return set(lst)


def parsePartnerProfit(row):
    agentCost = int(row[25])
    partnerCost = int(row[18])
    partnerProfit = int(row[19])
    transactionId = int(row[0])
    merchFee = int(row[24])

    dao = agentCost - merchFee
    if dao > 0:
        print('tiaomao {} dao{}'.format(transactionId, dao))
        partnerProfit += dao

    if partnerProfit != agentCost - partnerCost:
        return False

    return True


def newParsePartnerProfitWithError(errorSet):
    def parsePartnerProfit(row):
        #   `F_parter_cost_fee_bymillion` bigint(20) DEFAULT '0' COMMENT '合作方成本,百万分之一分', 18
        # `F_parter_profit_bymillion` bigint(20) DEFAULT '0' COMMENT '合作方收益,百万分之一分', 19

        # `F_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '交易时间', 20
        # `F_create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, 21
        # `F_channel_name` varchar(64) NOT NULL DEFAULT '0' COMMENT '通道名称', 22
        # `F_channel_mcc_code` varchar(20) NOT NULL DEFAULT '0' COMMENT '通道mcc', 23
        # `F_merchant_fee_bymillion` bigint(20) DEFAULT '0' COMMENT '商户手续费,百万分之一分', 24

        #  `F_agent_cost_fee_bymillion` bigint(20) DEFAULT '0' COMMENT '代理商成本,百万分之一分', 25
        agentCost = int(row[25])
        partnerCost = int(row[18])
        partnerProfit = int(row[19])
        transactionId = int(row[0])
        merchFee = int(row[24])

        if transactionId in errorSet:
            print('partner profit {}'.format(transactionId))

        dao = agentCost - merchFee
        if dao > 0:
            print('tiaomao {} dao{}'.format(transactionId, dao))
            partnerProfit += dao

        if partnerProfit != agentCost - partnerCost:
            return False

        return True

    return parsePartnerProfit


def newParseTiaoma(errorSet):
    def parseTiaoma(row):

        #(stPartnerTransProcedure.agent_cost_fee_bymillion-stPartnerTransProcedure.merch_fee_million);
        agentCost = int(row[28])
        partnerCost = int(row[30])
        partnerProfit = int(row[31])
        transactionId = int(row[0])
        merchFee = int(row[29])

        if transactionId in errorSet:
            print('tiaomao error {}'.format(transactionId))

        dao = agentCost - merchFee
        if dao > 0:
            print('tiaomao {} dao{}'.format(transactionId, dao))
            partnerProfit += dao

        if partnerProfit != agentCost - partnerCost:
            print('tiaomao partnerProfit {}'.format(transactionId, dao))

            return False

        return True

    return parseTiaoma


#F_merchant_fee_bymillion

#`F_transaction_id` varchar(32) NOT NULL DEFAULT '' COMMENT '交易id',0
#`F_order_id` varchar(32) NOT NULL COMMENT '乐刷订单号',1,
#   `F_date` date NOT NULL DEFAULT '2016-01-01' COMMENT '结算日期', 2
#   `F_operation_type` int(10) NOT NULL DEFAULT '0' COMMENT '流水类型:0--t1进账,1--t1出账,2--t0退票', 3
#   `F_agent_id_1g` varchar(32) DEFAULT '0' COMMENT '一级代理商编号', 4
#   `F_merchant_id` varchar(20) NOT NULL COMMENT '乐刷商户ID', 5
#   `F_payment_channel_id` int(10) unsigned DEFAULT NULL, 6
#   `F_payment_channel_merchant_id` varchar(64) DEFAULT NULL, 7
#   `F_payment_channel_terminal_id` varchar(32) DEFAULT NULL, 8
#   `F_amount` bigint(20) DEFAULT '0' COMMENT '交易额，单位分', 9
#   `F_card_type` int(10) NOT NULL DEFAULT '0' COMMENT '卡类型:0-未知，1-借记卡，2-贷记卡，3-境外卡', 10
#   `F_pay_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '1--pos,2-weixin,3-alipay,4-实名支付,5快捷支付', 11
#   `F_max_flag` tinyint(1) DEFAULT '0' COMMENT '封顶标志:0--非封顶,1--封顶', 12
#   `F_old_max_flag` tinyint(1) DEFAULT '0' COMMENT '封顶标志:0--非封顶,1--封顶', 13
#   `F_new_max_flag` tinyint(1) DEFAULT '0' COMMENT '封顶标志:0--非封顶,1--封顶', 14
#   `F_old_preferential_type` tinyint(4) DEFAULT '0' COMMENT '优惠类型：0:标准类，1：优惠类，2：减免类，3：特殊计费类', 15
#   `F_new_preferential_type` tinyint(4) DEFAULT '0' COMMENT '优惠类型：0:标准类，1：优惠类，2：减免类，3：特殊计费类', 16
#   `F_old_channel_commission_bymillion` int(10) unsigned DEFAULT '0' COMMENT '通道费率,百万分之一', 17
#   `F_old_channel_fee_bymillion` bigint(20) DEFAULT '0' COMMENT '通道手续费,百万分之一分', 18
#   `F_new_channel_commission_bymillion` int(10) unsigned DEFAULT '0' COMMENT '跳码后通道费率, 百万分之一', 19
#   `F_new_channel_fee_bymillion` bigint(20) DEFAULT '0' COMMENT '跳码后通道手续费,百万分之一分', 20
#   `F_total_gap_channel_fee_bymillion` bigint(20) DEFAULT '0' COMMENT '跳码差额通道手续费,百万分之一分', 21
#   `F_ls_divide_cent` int(10) DEFAULT '100' COMMENT '跳码乐刷分成比例', 22
#   `F_ls_gap_channel_fee_bymillion` bigint(20) DEFAULT '0' COMMENT '跳码差额通道手续费,百万分之一分', 23
#   `F_gap_channel_fee_bymillion` bigint(20) DEFAULT '0' COMMENT '跳码差额通道手续费,百万分之一分', 24
#   `F_create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, 25
#   `F_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '交易时间', 26
#   `F_partner_commission_bymillion` int(10) unsigned DEFAULT '0' COMMENT '合作方费率,百万分之一', 27
#   `F_agent_cost_fee_bymillion` bigint(20) DEFAULT '0' COMMENT '代理商成本,百万分之一分', 28

#   `F_merchant_fee_bymillion` bigint(20) DEFAULT '0' COMMENT '商户手续费,百万分之一分', 29
#   `F_partner_cost_fee_bymillion` bigint(20) DEFAULT '0' COMMENT 'partner商成本,百万分之一分', 30

#   `F_partner_profit_bymillion` bigint(20) DEFAULT '0' COMMENT '合作方收益=代理商成本-合作方成本,百万分之一分', 31

#   `F_partner_daogua_bymillion` bigint(20) DEFAULT '0' COMMENT '倒挂金额,百万分之一分', 32
#   `F_partner_cost_max_fee_million` int(10) unsigned DEFAULT '0' COMMENT '合作方封顶成本,百万分之一分',33
#   `F_agent_cost_commission_bymillion` bigint(20) DEFAULT '0' COMMENT '代理商成本费率,百万分之一分',34
#   `F_merchant_commission_bymillion` bigint(20) DEFAULT '0' COMMENT '商户费率,百万分之一',35


def parsePartnerPaybackProfit(row):

    #    `F_agent_cost_fee_bymillion` bigint(20) DEFAULT '0' COMMENT '代理商成本,百万分之一分', 17
    # `F_partner_cost_fee_bymillion` bigint(20) DEFAULT '0' COMMENT '合作方总成本,百万分之一分', 18
    # `F_partner_profit_bymillion` bigint(20) DEFAULT '0' COMMENT '本合作方实际分成=代理商成本-合作方总成本', 19 = 17 - 18
    agentCost = int(row[17])
    partnerCost = int(row[18])
    partnerProfit = int(row[19])
    if partnerProfit != agentCost - partnerCost:
        return False
    return True


if __name__ == '__main__':
    print('hello')
    loadCvsFiles('t_shuabao_partner_profit_statement.csv',
                 't_shuabao_tiaoma_statement.csv',
                 't_shuabao_partner_payback_profit_statement.csv', 'error.txt')
